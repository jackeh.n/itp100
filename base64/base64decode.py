def base64decode(chars):
    """
      >>> base64decode('STOP')
      b'I3\\x8f'
    """
    try:
        base = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
        int = [base.index(ch) for ch in chars]
        v1 = (int[0] << 2) | ((int[1] & 48) >> 4)
        v2 = (int[1] & 15) << 4 | int[2] >> 2
        v3 = (int[2] & 3) << 6 | (int[3] & 63)
        return bytes([v1, v2, v3])
    except:
        print("no")

if __name__ == '__main__':
    import doctest
    doctest.testmod()