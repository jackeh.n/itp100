from gasp import *
counter = 0
correct = 0
if counter < 10:
    num1 = random_between(1, 10)
    num2 = random_between(1, 10)
    question = "What is " + str(num1) + " times " + str(num2) + "?"
    result = read_number(question)
    answer = num1 * num2
    if result == answer:
        print("correct!")
        counter += 1
        correct += 1
    else:
        print("incorrect.")
        counter += 1
print("you got " + str(correct) + " out of 10 correct")