from gasp import *

begin_graphics()

#head
Circle((300, 200), 40)

#mouth
Arc((300, 200), 30, 225, 315)

#eyes
Circle((285, 210), 5)
Circle((315, 210), 5)

#nose
Line((290, 190), (300, 210))
Line((290, 190), (310, 190))

#body arms legs
Line((300, 160), (300, 60))
Line((270, 140), (300, 120))
Line((300, 120), (330, 140))
Line((280, 0), (300, 60))
Line((300, 60), (320, 0))

update_when('key_pressed')
end_graphics()